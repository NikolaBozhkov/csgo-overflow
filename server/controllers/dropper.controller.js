'use strict';
let User = require('../models/user'),
    userController = require('./user.controller'),
    io = require('../config/socket').io,
    Chance = require('chance'),
    winston = require('winston'),
    moment = require('moment'),
    Stats = require('../models/stats'),
    crypto = require('crypto'),
    request = require('request');

const DROPS = 4;
var ready = {};

module.exports = {
    getDrops: function (req, res) {
        userController.loadAllUsers((err, users, usersIndex) => {
            if (err) return userController.defaultErrHandle(err, res);

            let user = usersIndex[req.decoded.id];
            if (user) {
                // Validate if user is actually on the site
                if (!userController.userSocketPairs[user.id] || !ready[user.id]) {
                    return res.json({success: 0, message: 'Cheating results in a ban, better stop it!'});
                }

                // Normal human reactions are 500ms
                if (moment.utc().valueOf() - ready[user.id].time < 500) {
                    return res.json({ success: 0, message: 'Slow down, you are going too fast!' });
                }

                dropsGiven += DROPS;
                ready[user.id].status = 0;
                user.update({ staticCurrency: user.staticCurrency + DROPS }, (err) => {
                    if (err) return errorHandler(err, res, user.id);

                    user.staticCurrency += DROPS;
                    return res.json({success: 1, drops: DROPS});
                });
            } else {
                return res.status(404).json({ message: 'No user with id: ' + req.decoded.id });
            }
        });
    },

    onAdLoaded: function (userId) {
        ready[userId] = {
            status: 1,
            time: moment.utc().valueOf()
        };
    },

    adscendPostback: function(req, res) {
        let data = 'userId=' + req.params.userId + '/ip=' + req.params.ip;
        let hash = crypto.createHmac('md5', 'NzhkZDM0OWNiY2I3NTQ0ZTdiYjk1Yjc0ODJhYjVjYTI=').update(data);

        if (req.params.hash === hash.digest('hex')) {
            return module.exports.onPostbackValid(req, res, 'adscend');
        } else {
            return res.status(400).json({ status: 0, message: 'Bad request' });
        }
    },

    offerToroPostback: function(req, res) {
        let data = req.params.oid + '-' + req.params.userId + '-' + '7f88c7b1435510421c8b6f0b3d2bf47b';
        let hash = crypto.createHash('md5').update(data).digest("hex");

        if (req.params.sig === hash) {
            return module.exports.onPostbackValid(req, res, 'offertoro');
        } else {
            return res.status(400).json({ status: 0, message: 'Bad request' });
        }
    },

    personalyPostback: function(req, res) {
        let data = req.params.userId + ':' + '5b4cac7028df40d659f0d55c5168d437' + ':' + '4c084a10424484734583d2d8bae33b23';
        let hash = crypto.createHash('md5').update(data).digest("hex");

        if (req.params.hash === hash) {
            return module.exports.onPostbackValid(req, res, 'personaly');
        } else {
            return res.status(400).json({ status: 0, message: 'Bad request' });
        }
    },

    adgatePostback: function(req, res) {
        if (req.params.secret === 'NzhkZDM0OWNiY2I3NTQ0ZTdiYjk1Yjc0ODJhYjVjYTI=') {
            return module.exports.onPostbackValid(req, res, 'adgate');
        } else {
            return res.status(400).json({ status: 0, message: 'Bad request' });
        }
    },

    onPostbackValid: function(req, res, company) {
        userController.loadAllUsers((err, users, usersIndex) => {
            if (err) return userController.defaultErrHandle(err, res);

            request('http://check.getipintel.net/check.php?ip=' + req.params.ip + '&contact=bozhkov.nikola@gmail.com&flags=f', (err, resIp, body) => {
                let user = usersIndex[req.params.userId];
                // log Object
                let log = {
                    company: company,
                    ip: req.params.ip,
                    status: req.params.status,
                    currency: req.params.currency,
                    offerName: req.params.oname,
                    offerId: req.params.oid
                };

                // if ip is not good don't process payment and flag user
                if (req.params.ip && body > 0.9 && user) {
                    user.surveyLogs.push(log);
                    user.update({ flag: 1, surveyLogs: user.surveyLogs }, (err) => {
                        if (err) return module.exports.errorHandler(err, res, 'flag_failed', 'userId: ' + user.id);
                        user.flag = 1;
                        return res.status(200).send('1');
                    });
                } else if (user) {
                    // get new currency value after payment
                    let currency = parseInt(req.params.currency);
                    if (currency > 0 && req.params.status && req.params.status != 1) {
                        currency *= -1;
                    }

                    let newCurrency = user.staticCurrency + currency;

                    // If offer is more than $2 payout, flag the log and user for security
                    if (currency > 5000 || currency < -5000) {
                        log.flag = 1;
                        user.update({ flag: 1 }, (err) => {
                            if (err) return module.exports.errorHandler(err, res, 'flag_failed', 'userId: ' + user.id);
                            user.flag = 1;
                        });
                    }

                    user.surveyLogs.push(log);
                    user.update({ staticCurrency: newCurrency, surveyLogs: user.surveyLogs }, (err) => {
                        if (err) return module.exports.errorHandler(err, res, 'postback_user_update', 'userId: ' + user.id + ' currency: ' + currency);
                        user.staticCurrency = newCurrency;
                        return res.status(200).send('1');
                    });
                } else {
                    return res.status(200).send('1');
                }
            });
        });
    },

    errorHandler: function (err, res, log, msg) {
        winston.log(log, msg + ' message: ' + err.message);
        return res.status(500).json({ message: err.message });
    }
}
