import { RouterModule } from '@angular/router';
import { RerouterService } from './rerouter/rerouter.service';
import { AccountComponent } from './user/account.component';
import { DropperComponent } from './dropper/dropper.component';
import { MarketplaceComponent } from './marketplace/marketplace.component';
import { GameComponent } from './game/game.component';
import { StatisticsComponent } from './statistics/statistics.component';
import { TermsOfUseComponent } from './info/terms-of-use.component';
import { ContactsComponent } from './info/contacts.component';
import { FaqComponent } from './info/faq.component';
import { ReworkComponent } from './rework/rework.component';
var routes = [
    {
        path: 'hidden/wip/account',
        canActivate: [RerouterService],
        component: AccountComponent
    },
    {
        path: 'hidden/wip/advertisements',
        component: DropperComponent
    },
    {
        path: 'hidden/wip/marketplace',
        component: MarketplaceComponent
    },
    {
        path: 'hidden/wip/game',
        component: GameComponent
    },
    {
        path: 'hidden/wip/statistics',
        canActivate: [RerouterService],
        component: StatisticsComponent
    },
    {
        path: 'hidden/wip/faq',
        component: FaqComponent,
    },
    {
        path: 'hidden/wip/faq/:refId',
        component: FaqComponent,
    },
    {
        path: 'hidden/wip/terms/:option',
        pathMatch: 'full',
        component: TermsOfUseComponent
    },
    {
        path: 'hidden/wip/contacts',
        component: ContactsComponent
    },
    {
        path: 'hidden/wip/:refId',
        redirectTo: 'faq/:refId'
    },
    {
        path: '',
        pathMatch: 'full',
        component: ReworkComponent
    },
    {
        path: '**',
        redirectTo: ''
    }
];
export var routing = RouterModule.forRoot(routes);
//# sourceMappingURL=../../../src/app/app.routes.js.map